import numpy as np
import multicopter_model.constants as cs
from multicopter_model.constants import FlightMode, States 
from multicopter_model.pid import PID

class QuadCopterController:
    def __init__(self):
        self.target_x = 0
        self.target_y = 0
        self.target_z = 10
        self.target_yaw = 0
        
        self.position_controller_x = PID()
        self.position_controller_y = PID()
        self.position_controller_z = PID()

        self.velocity_controller_x = PID()
        self.velocity_controller_y = PID()
        self.velocity_controller_z = PID()

        self.roll_controller = PID()
        self.pitch_controller = PID()
        self.yaw_controller = PID()

        self.roll_rate_controller = PID()
        self.pitch_rate_controller = PID()
        self.yaw_rate_controller = PID()

        # Пример простого маршрута
        self._mission = [[0, 0, 10 , 0], [0, 10, 10, 1]]
        self._current_mission_index = 0


    def set_target_position(self, x, y, z, yaw):
        self.target_x = x
        self.target_y = y
        self.target_z = z
        self.target_yaw = yaw


    def update(self, state_vector, dt) -> np.ndarray:

        #TODO Обновление целевой точки маршрута

        #TODO Расчет целевой скорости ЛА


        #TODO Расчет тяги и углов крена и тангажа, перепроектирование углов в связную СК
        target_roll = 0
        target_pitch = 0
        cmd_trust = 1000
        
        # Пример для контура управления угловым положением и угловой скоростью.
        target_roll_rate = self.roll_controller.update(state_vector[States.ROLL], -target_roll, dt)
        target_pitch_rate = self.pitch_controller.update(state_vector[States.PITCH], target_pitch, dt)
        target_yaw_rate = self.yaw_controller.update(state_vector[States.YAW], self.target_yaw, dt)

        cmd_roll = self.roll_rate_controller.update(state_vector[States.ROLL_RATE], target_roll_rate, dt)
        cmd_pitch = self.pitch_rate_controller.update(state_vector[States.PITCH_RATE], target_pitch_rate, dt)
        cmd_yaw = self.yaw_rate_controller.update(state_vector[States.YAW_RATE], target_yaw_rate, dt)

        u = self._mixer(cmd_trust, cmd_roll, cmd_pitch, cmd_yaw)
        return u

    def _mixer(self, cmd_trust, cmd_roll, cmd_pitch, cmd_yaw) -> np.ndarray:
        u_1 = 0 #TODO Реализуйте алгоритм смешивания команд
        u_2 = 0 #TODO 
        u_3 = 0 #TODO 
        u_4 = 0 #TODO 
        return np.array([u_1, u_2, u_3, u_4])

    def _rotation2d(self, theta):
        return np.array([[np.cos(theta), -np.sin(theta)], 
                         [np.sin(theta),  np.cos(theta)]])